---
license: other
---


![Aquila_logo](./log.jpeg)


<h4 align="center">
    <p>
        <a href="https://huggingface.co/BAAI/AquilaChat2-70B-Expr/blob/main/README.md">English</a> 
        <b>简体中文</b> |
    </p>
</h4>

# 悟道·天鹰（Aquila2）

我们开源了我们的 **Aquila2** 系列，现在包括基础语言模型 **Aquila2-7B**，**Aquila2-34B** 和 **Aquila2-70B-Expr** ，对话模型 **AquilaChat2-7B**，**AquilaChat2-34B** 和**AquilaChat2-70B-Expr** ，长文本对话模型**AquilaChat2-7B-16k** 和 **AquilaChat2-34B-16k**

悟道 · 天鹰 Aquila 模型的更多细节将在官方技术报告中呈现。请关注官方渠道更新。

## 快速开始使用

## 使用方式/How to use

### 1. 推理/Inference

```python
import torch
from transformers import AutoTokenizer, AutoModelForCausalLM
from transformers import BitsAndBytesConfig

model_info = "BAAI/AquilaChat2-70B-Expr"
tokenizer = AutoTokenizer.from_pretrained(model_info, trust_remote_code=True)
model = AutoModelForCausalLM.from_pretrained(model_info, trust_remote_code=True, torch_dtype=torch.bfloat16)
model.eval()
text = "请给出10个要到北京旅游的理由。"
from predict import predict
out = predict(model, text, tokenizer=tokenizer, max_gen_len=200, top_p=0.95,
              seed=1234, topk=100, temperature=0.9, sft=True,
              model_name="AquilaChat2-70B")
print(out)
```


## 证书/License

Aquila2系列开源模型使用 [智源Aquila系列模型许可协议](https://huggingface.co/BAAI/AquilaChat2-70B-Expr/blob/main/BAAI-Aquila-70B-Model-License-Agreement.pdf)